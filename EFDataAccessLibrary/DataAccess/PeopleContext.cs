﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;


namespace EFDataAccessLibrary.DataAccess
{
    public class PeopleContext : DbContext 
    {
        public PeopleContext(DbContextOptions options) : base(options) { }

        //Here is we setup our  essential tables
        public DbSet<Models.Person> People { get; set; }
        public DbSet<Models.Address> Addresses { get; set; }
        public DbSet<Models.Email> EmailAddresses { get; set; }
             
    }
}
